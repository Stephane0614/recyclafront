import {RouterModule, Routes} from "@angular/router";
import {LoginPageComponent} from "./template/login-page/login-page.component";
import {HomePageComponent} from "./template/home-page/home-page.component";
import {MaterialCardComponent} from "./component/material-card/material-card.component";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent
  },
  {
    path: 'homePage',
    component: HomePageComponent
  },
  {
    path: 'materialCard/:id',
    component: MaterialCardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
