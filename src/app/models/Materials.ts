import {Matter} from "./Matter";

export interface Materials {
  id: number;
  name: string;
  matter: Matter[];
  image: string;
  recyclaScore: number;
}
