import {TestBed} from '@angular/core/testing';
import {HomePageComponent} from './home-page.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MaterialService} from '../../service/MaterialService';
import {RouterTestingModule} from '@angular/router/testing';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('HomePageComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatToolbarModule,
        MatCardModule,
        MatButtonModule,
        MatSelectModule,
        MatMenuModule,
        MatIconModule
      ],
      declarations: [HomePageComponent],
      providers: [MaterialService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  it('should create', () => {
    const fixture = TestBed.createComponent(HomePageComponent);
    const component = fixture.componentInstance;
    expect(component).toBeTruthy();
  });
});
