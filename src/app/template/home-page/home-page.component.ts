import {Component, OnInit} from '@angular/core';
import {Materials} from "../../models/Materials";
import {MaterialService} from "../../service/MaterialService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  materialsList: Materials[] = [];
  selectedMaterial: Materials | undefined;

  constructor(
    private materialService: MaterialService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.materialService.getMaterials().subscribe((materials: Materials[]) => this.materialsList = materials);
  }

  getMaterialsCard(material: Materials): void {
    this.router.navigate(['/materialCard', material.id]);
  }
}
