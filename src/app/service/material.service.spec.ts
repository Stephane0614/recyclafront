import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Materials } from '../models/Materials';
import {MaterialService} from "./MaterialService";

describe('MaterialService', () => {
  let service: MaterialService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MaterialService]
    });
    service = TestBed.inject(MaterialService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve all materials from the API via GET', () => {
    const dummyMaterials: Materials[] = [
      { id: 1, name: 'Material 1', matter: [], image: 'image1.png', recyclaScore: 0 },
      { id: 2, name: 'Material 2', matter: [], image: 'image2.png', recyclaScore: 0 },
    ];

    service.getMaterials().subscribe((materials: string | any[]) => {
      expect(materials.length).toBe(2);
      expect(materials).toEqual(dummyMaterials);
    });

    const request = httpMock.expectOne(`${service['apiUrl']}`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyMaterials);
  });

  it('should retrieve a material by ID from the API via GET', () => {
    const dummyMaterial: Materials = { id: 1, name: 'Material 1', matter: [], image: 'image1.png', recyclaScore: 0 };

    service.getMaterial(1).subscribe((material: any) => {
      expect(material).toEqual(dummyMaterial);
    });

    const request = httpMock.expectOne(`${service['apiUrl']}/get/1`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyMaterial);
  });

  it('should retrieve the recycling score for a material by ID from the API via GET', () => {
    const dummyScore = 80;

    service.getRecyclingScore(1).subscribe((score: any) => {
      expect(score).toBe(dummyScore);
    });

    const request = httpMock.expectOne(`${service['apiUrl']}/recycling-score/1`);
    expect(request.request.method).toBe('GET');
    request.flush(dummyScore);
  });
});
