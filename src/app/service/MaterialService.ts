import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Materials} from "../models/Materials";

@Injectable({
  providedIn: 'root'
})

export class MaterialService {

  private apiUrl = 'http://localhost:8080/api/materials';

  constructor(private http: HttpClient) {
  }

  getMaterials(): Observable<Materials[]> {
    return this.http.get<Materials[]>(this.apiUrl);
  }

  getMaterial(id: number): Observable<Materials> {
    return this.http.get<Materials>(`${this.apiUrl}/get/${id}`);
  }

  getRecyclingScore(id: number): Observable<number> {
    return this.http.get<number>(`${this.apiUrl}/recycling-score/${id}`);
  }

  createMaterial(material: Materials): Observable<Materials> {
    return this.http.post<Materials>(this.apiUrl, material);
  }

  updateMaterial(id: number, material: Materials): Observable<Materials> {
    return this.http.put<Materials>(`${this.apiUrl}/update/${id}`, material);
  }

  deleteMaterial(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/delete/${id}`);
  }
}
