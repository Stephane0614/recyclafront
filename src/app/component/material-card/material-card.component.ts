import {Component, Input, OnInit} from '@angular/core';
import {Materials} from "../../models/Materials";
import {ActivatedRoute} from "@angular/router";
import {MaterialService} from "../../service/MaterialService";

@Component({
  selector: 'app-material-card',
  templateUrl: './material-card.component.html',
  styleUrls: ['./material-card.component.css']
})
export class MaterialCardComponent implements OnInit {
@Input() materials!: Materials | undefined;
  materialsList: Materials[] = [];
  recyclingScore: number | undefined;

  constructor(
    private route: ActivatedRoute,
    private materialService: MaterialService
) {}

  ngOnInit() {
    this.materialService.getMaterials().subscribe((materials: Materials[]) => {
      this.materialsList = materials;
      this.route.params.subscribe(params => {
        const materialId = +params['id'];
        this.materials = this.getMaterialById(materialId);
        if (this.materials) {
          this.getRecyclingScore(this.materials.id);
        }
      });
    });
  }

  getMaterialById(materialId: number): Materials | undefined {
    return this.materialsList.find(material => material.id === materialId);
  }

  getRecyclingScore(id: number): void {
    this.materialService.getRecyclingScore(id).subscribe((score: number | undefined) => {
      this.recyclingScore = score;
    });
  }
}
