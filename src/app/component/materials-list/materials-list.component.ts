import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Materials} from "../../models/Materials";

@Component({
  selector: 'app-materials-list',
  templateUrl: './materials-list.component.html',
  styleUrls: ['./materials-list.component.css']
})
export class MaterialsListComponent  {
  @Input() materialList!: Materials[];
  @Output() getMaterialsCardEmitter:EventEmitter<Materials> = new EventEmitter<Materials>();

  getMaterialsCardEmit(material: Materials) {
    this.getMaterialsCardEmitter.emit(material);
  }
}
