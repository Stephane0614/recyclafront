module.exports = function (config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine", "@angular-devkit/build-angular"],
    plugins: [
      require("karma-jasmine"),
      require("karma-chrome-launcher"),
      require("karma-jasmine-html-reporter"),
      require("karma-coverage"),
      require("@angular-devkit/build-angular/plugins/karma"),
    ],
    browsers: ["GitlabHeadlessChrome"],
    customLaunchers: {
      GitlabHeadlessChrome: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },
    singleRun: true,  // Assure que Karma se ferme après l'exécution des tests
    autoWatch: false,  // Désactive la surveillance des fichiers pour les changements
    reporters: ['progress', 'kjhtml'],  // Ajout des reporters
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    }
  });
}
