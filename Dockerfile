# Étape 1 : Construction de l'application Angular
FROM node:16 AS build

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install

COPY . ./
RUN npm run build --prod

# Étape 2 : Configuration de NGINX pour servir l'application Angular
FROM nginx:alpine

# Copier les fichiers de construction d'Angular vers le dossier de NGINX
COPY --from=build /app/dist /usr/share/nginx/html

# Copier le fichier de configuration personnalisé de NGINX (facultatif)
COPY nginx.conf /etc/nginx/nginx.conf

# Exposer le port 80
EXPOSE 80

# Commande de démarrage de NGINX
CMD ["nginx", "-g", "daemon off;"]
